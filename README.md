# ics-ans-role-gitlab-bot

Ansible role to install gitlab-bot, a generic ESS GitLab bot.

## Role Variables

```yaml
gitlab_bot_network: gitlab-bot-network
gitlab_bot_image: registry.esss.lu.se/ics-infrastructure/gitlab-bot
gitlab_bot_tag: latest
gitlab_bot_gitlab_listener_secret: "secret"
# GitLab webhook secret
gitlab_bot_gl_secret: "secret"
# GitLab token to access the API
gitlab_bot_gl_access_token: "mytoken"
gitlab_bot_frontend_rule: "Host:{{ ansible_fqdn }}"
gitlab_bot_sentry_dsn: ""
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-gitlab-bot
```

## License

BSD 2-clause
